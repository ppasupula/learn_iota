

### Getting started

```
npm install
```

run sample scripts, all are independent

```

node attach-address-tangle.js

node transactions-example.js

node test-confirmation-times.js

```


### Get the node via docker

```
docker run -e REMOTE_LIMIT_API="getNeighbors, addNeighbors, removeNeighbors" -p 14265:14265 -p 15600:15600 -p 14600:14600/udp -v /Users/phaninderpasupula/iota-data:/iri/data --name iri iotaledger/iri:latest --remote -p 14265
```
