const loki = require('lokijs')
const axios = require('axios')
const moment = require('moment')
const Bluebird = require('bluebird')

var db = new loki('data.json')

const exploreHost = 'https://api.thetangle.org'

function start() {

  var txCollection
  db.loadDatabase({}, async function () {
    txCollection = db.getCollection('tx')

    for (let index = 0; index < txCollection.data.length; index++) {
      const tx = txCollection.data[index];
      if (tx.confirm) {
        continue
      }

      let confirmingTimestamp = await getTransactionConfTime(tx.tail)
      await Bluebird.delay(1000)
      if (!confirmingTimestamp && tx.reattach) {
        confirmingTimestamp = await getTransactionConfTime(tx.reattachHash)
        if (!confirmingTimestamp) {
          console.log('reattached but not confirmed', { reattachHash: tx.reattachHash })
        }
      }

      if (confirmingTimestamp) {
        confirmingTimestamp = confirmingTimestamp.unix()
        console.log({ confirmingTimestamp, hash: tx.tail })

        tx.confirm = true
        tx.confirmTime = confirmingTimestamp
        txCollection.update(tx)
      }
    }

    db.saveDatabase()
  });

}
// start()

const json2csv = require('json2csv').parse;

function saveToCSV() {
  db.loadDatabase({}, function () {
    txCollection = db.getCollection('tx')

    txCollection.data = txCollection.data.map(t => {
      t.startDate = moment(t.startDate).format()
      t.timeTaken = null
      if (t.confirm) {
        t.timeTaken = calculateSeconds(t.startTime, moment.unix(t.confirmTime))
      }
      return t
    })


    const fields = ['minWeightMagnitude', 'depth', 'tail', 'confirm', 'timeTaken', 'startDate', 'promoted', 'reattach', 'reattachHash'];
    const opts = { fields };

    try {
      const csv = json2csv(txCollection.data, opts);
      console.log(csv);
    } catch (err) {
      console.error(err);
    }
  })
}
saveToCSV()

function calculateSeconds(startDate, endDate) {
  // console.log(startDate, endDate)
  var start_date = moment(startDate);
  var end_date = moment(endDate);
  var duration = moment.duration(end_date.diff(start_date));
  var seconds = duration.asSeconds()
  return seconds;
}

async function getTransactionConfTime(hash) {

  let confirmed = false
  let res

  try {
    res = await axios.get(`${exploreHost}/v1/transactions/${hash}`)
  } catch (error) {
    console.log('transaction not found')
    // do nothing
  }

  // console.log(res.data)
  if (res && res.data.status === 'confirmed') {
    confirmed = true
  } else {
    console.log('transaction is not confirmed', hash)
  }

  if (confirmed) {
    return moment.unix(res.data.confirmingTimestamp)
  } else {
    return null
  }
}