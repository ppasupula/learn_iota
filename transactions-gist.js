const { composeAPI } = require('@iota/core') //1.0.0-beta.5
const { asciiToTrytes } = require('@iota/converter') //1.0.0-beta.5

const iota = composeAPI({
  provider: 'https://nodes.devnet.iota.org:443'
})

async function makeTx(seed, address) {
  const transfers = [{
    address: address,
    value: 1,
    tag: asciiToTrytes('node-tx'),
    message: asciiToTrytes('Hello World!')
  }]

  const minWeightMagnitude = 9 //devnet
  const depth = 3

  // bundle prep for all transfers
  const trytes = await iota.prepareTransfers(seed, transfers)
  const bundle = await iota.sendTrytes(trytes, depth, minWeightMagnitude)
  console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
  console.log(`Explorer link: https://devnet.thetangle.org/transaction/${bundle[0].hash}`)
}


const senderSeed = 'XDETDPOUHPRFA9GBTNTPSYWPZVHVSJQP9DZHF9YMOLPIDHYMHHNMDJLQZM9KGMZAZSUQQ9JWRBWYJLZPU'
const addressTo = 'R9MMWUXLBGPSWUMHDIGFLPINNRNKU9FTJJIOTPWSUITIXCNTDTUWROEVTXQBRQLXNJMEUWQATHZAGVZTD'

makeTx(senderSeed, addressTo)