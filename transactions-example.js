const { composeAPI } = require('@iota/core')
const { asciiToTrytes } = require('@iota/converter')

const iota = composeAPI({
  provider: 'https://nodes.devnet.iota.org:443'
})

const senderSeed = 'XDETDPOUHPRFA9GBTNTPSYWPZVHVSJQP9DZHF9YMOLPIDHYMHHNMDJLQZM9KGMZAZSUQQ9JWRBWYJLZPU'
const receiverSeed = 'BMTTSMUFDJWFXY9MYKXAQDESIUOBGTNQRQS9RPJAYCRCPCRREAHNMWJUTFYOP9WNAXQTNIKNMCRNSYSTF'

async function start() {
  // See node info
  const nodeInfo = await iota.getNodeInfo()
  console.log({nodeInfo})

  // generate address
  const addressTo = await iota.getNewAddress(receiverSeed)
  console.log({ addressTo })

  const transfers = [{
    address: addressTo,
    value: 0,
    tag: asciiToTrytes('test'),
    message: asciiToTrytes('Hello World!')
  },
  {
    address: addressTo,
    value: 0,
    tag: asciiToTrytes('test'),
    message: asciiToTrytes('Hello World2!')
  }]

  const minWeightMagnitude = 9 //devnet
  const depth = 3

  // bundle prep for all transfers
  const trytes = await iota.prepareTransfers(senderSeed, transfers)
  const bundle = await iota.sendTrytes(trytes, depth, minWeightMagnitude)
  console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
  console.log('Bundle:', bundle)
  console.log('\n', `see it here https://devnet.thetangle.org/transaction/${bundle[0].hash}`, '\n')
}

start()
