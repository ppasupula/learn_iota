const { composeAPI } = require('@iota/core')

const iota = composeAPI({
  provider: 'https://nodes.devnet.iota.org'
})

// const seed = 'XDETDPOUHPRFA9GBTNTPSYWPZVHVSJQP9DZHF9YMOLPIDHYMHHNMDJLQZM9KGMZAZSUQQ9JWRBWYJLZPU'
// const seed = 'GCMPFUFCIRZXTRTVMNCCRKZQCAAIJZSAVBQHYO9OSUNPKXMEQISHJYBSY9DMWEDAQSINNFQPHQYOKLURD'

// Attaching an address is just sending a 0 value tx to this address.
async function start() {

  // generate address
  const addressToAttach = await iota.getNewAddress(seed)
  console.log({ addressToAttach })

  const transfers = [{
    address: addressToAttach,
    value: 0
  }]

  const minWeightMagnitude = 9 //devnet
  const depth = 3

  // bundle prep for all transfers
  const trytes = await iota.prepareTransfers(seed, transfers)
  const bundle = await iota.sendTrytes(trytes, depth, minWeightMagnitude)
  console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
  console.log('Bundle:', bundle)
  console.log('\n', `see tx here https://devnet.thetangle.org/transaction/${bundle[0].hash}`, '\n')

  console.log('\n', `see address here https://devnet.thetangle.org/address/${bundle[0].address}`, '\n')

  console.log('\n get the address and add iota from faucet https://faucet.devnet.iota.org \n\n' )
}

start()
