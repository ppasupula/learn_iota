const { composeAPI } = require('@iota/core')
const { asciiToTrytes } = require('@iota/converter')
const Bluebird = require('bluebird')
const axios = require('axios')
const moment = require('moment')
const loki = require('lokijs')

var db = new loki('data.json', { autosave: true })
var txCollection = db.addCollection('tx');


// const provider = 'https://nodes.devnet.iota.org'  // testnet
// const exploreHost = 'https://api.devnet.thetangle.org'
// const minWeightMagnitude = 9

const provider = 'http://node02.iotatoken.nl:14265/' // livenet
const exploreHost = 'https://api.thetangle.org'
const minWeightMagnitude = 14

const depth = 3
const senderSeed = 'XDETDPOUHPRFA9GBTNTPSYWPZVHVSJQP9DZHF9YMOLPIDHYMHHNMDJLQZM9KGMZAZSUQQ9JWRBWYJLZPU'

const iota = composeAPI({ provider })

async function makeTx(count) {
  const addressTo = 'IETGETEQSAAJUCCKDVBBGPUNQVUFNTHNMZYUCXXBFXYOOOQOHC9PTMP9RRIMIOQRDPATHPVQXBRXIKFDDRDPQDBWTY'
  console.log({ addressTo })

  const transfers = []
  for (let index = 0; index < count; index++) {
    transfers.push({
      address: addressTo,
      value: 0,
      tag: asciiToTrytes('test-speed'),
      message: asciiToTrytes(`Hi ${index}`)
    })
  }

  // bundle prep for all transfers
  const startTime = Date.now()
  console.log('started tx with count:', count, startTime)
  const trytes = await iota.prepareTransfers(senderSeed, transfers)
  const bundle = await iota.sendTrytes(trytes, depth, minWeightMagnitude)
  console.log(bundle)
  console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
  // console.log('Bundle:', bundle)
  console.log('\n', `see it here ${exploreHost}/transaction/${bundle[0].hash}`, '\n')
  return {
    bundle, startTime, transfers
  }
}

async function start() {

  let retryRes
  let confirmTime
  let res
  let txDoc

  const timeout = 5 * 60 * 1000
  setTimeout(async function run() {
    try {
      res = await makeTx(1)
      txDoc = txCollection.insert({
        provider,
        minWeightMagnitude,
        depth,
        tail: res.bundle[0].hash,
        tails: res.bundle.map(t => t.hash),
        startTime: res.startTime,
        confirm: false,
        confirmTime: null,
        promoted: false,
        reattach: false,
        reattachHash: ''
      });
      confirmTime = await getTransactionConfTime2(res.bundle[0].hash)
      txDoc.confirm = !!confirmTime
      txDoc.confirmTime = confirmTime || null

      if (!confirmTime) {
        console.log('taking long, try reattach/replay')
        retryRes = await retryTx(res.bundle, res.transfers)
        // confirmTime = await getTransactionConfTime2(res.bundle[0].hash)

        if (retryRes) {
          txDoc.promoted = retryRes.isPromotable
          txDoc.reattach = retryRes.reattached
          if (retryRes.reattachedBundle) {
            txDoc.reattachHash = retryRes.reattachedBundle[0].hash
          }
        }
      }

      txCollection.update(txDoc)
      db.saveDatabase()

      console.log('time took for 1 tx', calculateSeconds(res.startTime, confirmTime), 'seconds')

    } catch (error) {
      console.log('something wrong', error)
    }
    setTimeout(run, timeout);

  }, 0)

  // res = await makeTx(100)
  // confirmTime = await getTransactionConfTime2(res.bundle[0].hash)
  // console.log('time took for 100 txs', calculateSeconds(res.startTime, confirmTime), 'seconds')


}
start()


async function getTransactionConfTime2(hash) {

  let confirmed = false
  let timeToCheck = 3 * 60 * 1000
  let res

  while (!confirmed && timeToCheck > 0) {
    try {
      res = await axios.get(`${exploreHost}/v1/transactions/${hash}`)
    } catch (error) {
      console.log('transaction not found')
      // do nothing
    }
    
    // console.log(res.data)
    if (res && res.data.status === 'confirmed') {
      confirmed = true
    } else {
      console.log('transaction is not confirmed. try again in 20 sec')
      await Bluebird.delay(20000)
      timeToCheck = timeToCheck - 20000
    }
  }

  if (confirmed) {
    return moment.unix(res.data.confirmingTimestamp)
  } else {
    return null
  }
}

async function retryTx(bundle, transfers) {
  const tails = bundle.map(a => a.hash)

  let reattachedBundle
  let isPromotable
  let reattached = false

  try {

    const states = await iota.getLatestInclusion(tails)
    if (states.indexOf(true) === -1) {
      const tail = tails[tails.length - 1]
      isPromotable = await iota.isPromotable(tail)

      if (isPromotable) {
        await iota.promoteTransaction(tail, depth, minWeightMagnitude, transfers)
      } else {
        reattached = true
        reattachedBundle = await iota.replayBundle(tail, depth, minWeightMagnitude)
        console.log(reattachedBundle)
        bundle.push(reattachedBundle[0])
      }
    }
  } catch (error) {
    console.log('retry tx fail', error)
    return null
  }


  return { bundle, reattachedBundle, isPromotable, reattached}
}

function calculateSeconds(startDate, endDate) {
  // console.log(startDate, endDate)
  var start_date = moment(startDate);
  var end_date = moment(endDate);
  var duration = moment.duration(end_date.diff(start_date));
  var seconds = duration.asSeconds()
  return seconds;
}


  
async function getTransactionConfTime(hashes) {
  let confirmed = false
  hashes = [].concat(hashes)
  const states = await iota.getLatestInclusion(hashes)
  confirmed = states[0]
  if (confirmed) {
    const transaction = await iota.getTransactionObjects(hashes)
    console.log(transaction)
  } else {
    console.log('transaction is not confirmed. try again in 5 sec')
  }

}






// const OLD_IOTA = require('iota.lib.js')
// const oldIota = new OLD_IOTA({ provider })

// async function getTransactionConfTime(hash) {
//   let confirmed = false
//   oldIota.api.getLatestInclusion([hash], (error, inclusionStates) => {
//     if (error) {
//       console.error('getLatestInclusion error', error)
//     }
//     if (inclusionStates[0]) {
//       console.log('transaction is not confirmed. try again in 5 sec')
//     }
//   })
//   await Bluebird.delay(5000)

// }
// getTransactionConfTime('FK9WULUPUN9TWAB9PCRBMLIFCYFOSMVINSJEDFJHXTRTBLNRECVFGWLNQUNPPYHZWUJDCXUNTJCWUF999')


